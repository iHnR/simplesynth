note_name_bases = ["A","A#/Bb","B","C","C#/Db","D","D#/Eb","E","F","F#/Gb","G","G#/Ab"]

class Note:
    def __init__(self,name:str,octave:int,frequency:float,midi_number:int):
        self.name           = name 
        self.octave         = octave
        self.frequency      = frequency
        self.midi_number    = midi_number 

    def __str__(self):
        return f"name: {self.name}, octave: {self.octave}, frequency: {self.frequency}, midi_number: {self.midi_number}"

    def strcsv(self):
        return f"{self.name},{self.octave},{self.frequency},{self.midi_number}"

half_step:float = 2**(1/12)

notes = [Note(
            f"{note_name_bases[(i-21)%12]}",
            (i-12)//12,
            27.5*half_step**(i-21),
            i) 
        for i in range(128)]

def printf(*args,**kwargs):
    kwargs['end'] = ''
    print(*args,**kwargs)

def pad(n):
    return ' '*n

f0 = 8.1757989156437
half_step = 2**(1/12)
frequencies = [f0*half_step**i for i in range(128)]
print(frequencies)

def print_to_rusts():
    for note in notes:
        print(f'MidiNote{{ \
    name: "{note.name}"{pad(6-len(note.name))}, \
    octave: {note.octave}{pad(3-len(str(note.octave)))}, \
    frequency: {note.frequency}{pad(20-len(str(note.frequency)))}, \
    midi_number: {note.midi_number}{pad(3-len(str(note.midi_number)))} }},')
