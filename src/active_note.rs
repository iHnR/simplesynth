#![allow(unused)]
use std::{
    f32::consts::{E, TAU},
    hash::Hash,
};

use midimessage::{MidiNote, NOTES};

use crate::waves;

const MAX_VOLUME: f32 = 0.95;
pub const MIN_VOLUME: f32 = 0.02;
const TIME_CONSTANT: f32 = 3.0;

#[derive(Debug, Clone, Copy)]
pub struct ADSR {
    pub attack: f32,
    pub decay: f32,
    pub sustain: f32,
    pub release: f32,
}

#[derive(Debug, Clone, Copy)]
pub enum ADSRState {
    Rising,
    Decaying,
    Releasing,
}

/// The active note struct should be robust enough not to get confused when it's hit while still
/// releasing.
#[derive(Debug)]
pub struct ActiveNote<'a> {
    pub adsr: &'a ADSR,
    pub adsr_state: ADSRState,
    pub current_volume: f32,
    pub midi_note: &'a MidiNote<'static>,
    pub velocity: f32,
    pub current_phase: f32,
}

impl<'a> ActiveNote<'a> {
    pub fn new(midi_number: u8, velocity: u8, adsr: &'a ADSR) -> Self {
        Self {
            midi_note: &NOTES[midi_number as usize],
            velocity: E.powf((velocity as f32 / u8::MAX as f32 - 1.0) * TIME_CONSTANT),
            adsr_state: ADSRState::Rising,
            current_volume: 0.0,
            adsr,
            current_phase: 0.0,
        }
    }

    /// Just go to release mode.
    pub fn release(&mut self) {
        self.adsr_state = ADSRState::Releasing;
    }

    pub fn get_wave_value(&self, function: fn(f32) -> f32) -> f32 {
        self.velocity
            * self.current_volume
            * function(self.current_phase * self.midi_note.frequency * TAU)
    }

    /// Apply hit to note. Should only be possible while releasing.
    pub fn hit(&mut self) {
        self.adsr_state = ADSRState::Rising;
    }

    pub fn take_time_step(&mut self, dt: f32) {
        self.adsr_state = match (self.adsr_state, self.current_volume) {
            (ADSRState::Rising, volume) if volume > MAX_VOLUME => ADSRState::Decaying,
            _ => self.adsr_state,
        };
        self.current_volume = match self.adsr_state {
            ADSRState::Rising => {
                1.0 - (1.0 - self.current_volume) * E.powf(-dt * TIME_CONSTANT / self.adsr.attack)
            }
            ADSRState::Decaying => {
                self.adsr.sustain
                    + (self.current_volume - self.adsr.sustain)
                        * E.powf(-dt * TIME_CONSTANT / self.adsr.decay)
            }
            ADSRState::Releasing => {
                self.current_volume * E.powf(-dt * TIME_CONSTANT / self.adsr.release)
            }
        };
        let new_phase = self.current_phase + dt;
        self.current_phase = if new_phase < self.midi_note.wave_length {
            new_phase
        } else {
            new_phase - self.midi_note.wave_length
        }
    }

    /// Consumes self and does nothing
    pub fn destroy(mut self) {}
}

impl Eq for ActiveNote<'_> {}

impl PartialEq for ActiveNote<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.midi_note.midi_number == other.midi_note.midi_number
    }
}

impl Hash for ActiveNote<'_> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.midi_note.midi_number.hash(state);
    }
}
