use std::io;
use std::io::BufRead;

/// Function that takes a reference to a collection and allows the user to select an item.
/// Returns a reference to the selected item or None if no choice is made or an invalid choice is entered.
pub fn select_item_from_collection<T>(collection: &[T]) -> Option<&T>
where
    T: std::fmt::Display,
{
    match collection.len() {
        0 => None,
        1 => collection.get(0),
        _ => {
            // Display the options with corresponding numbers
            for (index, item) in collection.iter().enumerate() {
                println!("{}. {}", index + 1, item);
            }
            println!("Please select an item (0 to make no choice):");
            loop {
                let stdin = io::stdin();
                let choice: usize = match stdin.lock().lines().next() {
                    Some(Ok(input)) => match input.trim().parse() {
                        Ok(num) if num <= collection.len() => num,
                        _ => continue,
                    },
                    _ => continue,
                };
                if choice == 0 {
                    return None;
                }
                return collection.get(choice - 1);
            }
        }
    }
}

/// Function that takes ownership of a collection and allows the user to select an item.
/// Returns the selected item or None if no choice is made or an invalid choice is entered.
pub fn select_item_from_owned_collection<T, C>(collection: C) -> Option<T>
where
    T: std::fmt::Display,
    C: Into<Vec<T>>,
{
    let mut collection: Vec<T> = collection.into();

    match collection.len() {
        0 => None,
        1 => collection.pop(),
        _ => {
            // Display the options with corresponding numbers
            for (index, item) in collection.iter().enumerate() {
                println!("{}. {}", index + 1, item);
            }
            println!("Please select an item (0 to make no choice):");
            loop {
                let stdin = io::stdin();
                let choice: usize = match stdin.lock().lines().next() {
                    Some(Ok(input)) => match input.trim().parse() {
                        Ok(num) if num <= collection.len() => num,
                        _ => continue,
                    },
                    _ => continue,
                };
                if choice == 0 {
                    return None;
                }
                return Some(collection.remove(choice - 1));
            }
        }
    }
}
