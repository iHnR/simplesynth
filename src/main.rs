use crate::active_note::*;
use crate::synth::*;
use jack;
use midimessage;
use midimessage::ControlChangeMessage::*;
use midimessage::MidiMessage;
use midimessage::MidiMessage::*;
use std::collections::HashMap;
use std::io;
use std::io::Read;
use std::sync::mpsc;

mod active_note;
mod synth;
mod user_input;
mod waves;

fn main() {
    let buffer_size = 32;
    start_synth(buffer_size);
}

// Create the adsr
const MY_ADSR: ADSR = ADSR {
    attack: 0.005,
    decay: 0.2,
    sustain: 0.2,
    release: 0.5,
};

fn start_synth(mut buffer_size: u32) {
    // Define communication channel
    let (tx, rx) = mpsc::sync_channel(64);

    // Define the jack device
    let (client, _status) =
        match jack::Client::new("SimpleSynth", jack::ClientOptions::NO_START_SERVER) {
            Ok((client, _status)) => (client, _status),
            Err(some_error) => panic!("{some_error}"), // Panick if the client could not be created.
        };

    // Define the midi input
    let midi_in = match client.register_port("midi_in", jack::MidiIn::default()) {
        Ok(port) => port,
        Err(_) => panic!("Failed to create midi in port."),
    };
    let midi_in_name = match midi_in.name() {
        Ok(name) => name,
        Err(_) => panic!("Could not read midi_in name."),
    };

    // Define audio outputs
    let mut audio_out = client
        .register_port("audio", jack::AudioOut::default())
        .unwrap();
    let synth_audio_out_name = match audio_out.name() {
        Ok(name) => name,
        Err(_) => panic!("Could not read audio_out name."),
    };

    // This stores the notes that are currently being played.
    // Pressed notes and sustained notes have different behaviour in some cases
    // so they are stored in seperate containers.
    // In future versions it might make sense to store weather a not is
    // sustained in the ActiveNote struct, but I'm not sure. This works
    // quite nicely.
    let mut pressed_notes: HashMap<u8, ActiveNote> = HashMap::new();
    let mut sustained_notes: HashMap<u8, ActiveNote> = HashMap::new();
    let mut sustain_status = false;

    // Get the sample rate
    let mut sample_rate = client.sample_rate();
    let mut time_step = 1.0 / sample_rate as f32;
    println!("Sample rate: {sample_rate}");
    println!("  Time step: {time_step}");

    // Set the buffer size to the user defined value.
    println!("Set buffer size: {:?}", client.set_buffer_size(buffer_size));

    // Create a jack process handler.
    // Reading github I believe this might be deprecated soon.
    let handler = jack::ClosureProcessHandler::new(
        move |client: &jack::Client, process_scope: &jack::ProcessScope| -> jack::Control {
            // We need the time per sample regularly. I think it can dynamically change so I put it
            // inside the process handler.
            if sample_rate != client.sample_rate() {
                // Here goes code that's needed when the sample rate changes.
                // Might be incomplete at the moment.
                sample_rate = client.sample_rate();
                time_step = 1.0 / sample_rate as f32;
                let _ = tx.send(SystemMessage::InfoMessage(format!(
                    "Sample rate changed to {sample_rate}",
                )));
            }

            buffer_size = if buffer_size != client.buffer_size() {
                // TODO: handle buffer size changes
                client.buffer_size()
            } else {
                buffer_size
            };

            // handle midi messages
            for event in midi_in.iter(process_scope) {
                // Convert bytes into midi message enum
                let message = MidiMessage::from_bytes(event.bytes);

                // Send to main process
                let _ = tx.send(SystemMessage::MidiMessage(message));

                // Handle message
                match message {
                    NoteOn {
                        channel: _,
                        note: note_number,
                        vel,
                        bytes: _,
                    } => {
                        if let Some(mut active_note) = sustained_notes.remove(&note_number) {
                            active_note.hit(); // Changes state to rising
                            pressed_notes.insert(note_number, active_note);
                        } else {
                            pressed_notes
                                .insert(note_number, ActiveNote::new(note_number, vel, &MY_ADSR));
                        }
                    }
                    NoteOff {
                        channel: _,
                        note: note_number,
                        vel: _,
                        bytes: _,
                    } => {
                        if sustain_status {
                            if let Some(active_note) = pressed_notes.remove(&note_number) {
                                sustained_notes.insert(note_number, active_note);
                            }
                        } else if let Some(active_note) = pressed_notes.get_mut(&note_number) {
                            active_note.release();
                        }
                    }
                    ControlChange {
                        channel: _,
                        message_type,
                        value,
                        bytes: _,
                    } => match message_type {
                        SustainPedal => {
                            if value == 0 {
                                sustained_notes
                                    .iter_mut()
                                    .for_each(|(_number, active_note)| {
                                        active_note.release();
                                    });
                                sustain_status = false;
                            } else {
                                sustain_status = true;
                            }
                        } // <-Change the sustain status
                        AllNotes => pressed_notes
                            .iter_mut()
                            .chain(sustained_notes.iter_mut())
                            .for_each(|(_number, active_note)| {
                                active_note.release();
                            }), // <-Disable all notes
                        _ => {}
                    },
                    _ => {}
                }
            }

            // Put notes in buffer
            let buffer = audio_out.as_mut_slice(process_scope);
            for sample in buffer.iter_mut() {
                let value: f32 = pressed_notes
                    .iter_mut()
                    .chain(sustained_notes.iter_mut())
                    .map(|(&note_number, active_note)| {
                        let a_weight = waves::MIDI_VAL_TO_A_WEIGHT[note_number as usize];
                        active_note.take_time_step(time_step);
                        a_weight * active_note.get_wave_value(waves::square)
                    })
                    .sum();
                *sample = value * 0.3;
            }

            // Remove releaseing notes smaller than MIN_VOLUME
            pressed_notes.retain(|_, active_note| match active_note.adsr_state {
                ADSRState::Releasing => active_note.current_volume > MIN_VOLUME,
                _ => true,
            });
            sustained_notes.retain(|_, active_note| match active_note.adsr_state {
                ADSRState::Releasing => active_note.current_volume > MIN_VOLUME,
                _ => true,
            });

            // Tell jack to continue
            jack::Control::Continue
        },
    );

    // Activate the jack process
    let active_client = match client.activate_async((), handler) {
        Ok(active_client) => active_client,
        Err(_) => panic!(),
    };

    // Select audio port
    let mut audio_ports: Vec<String> = active_client
        .as_client()
        .ports(
            Some(".*playback_FL.*"),
            Some(".*audio.*"),
            jack::PortFlags::IS_INPUT,
        )
        .iter()
        .map(|p: &String| p.replace("_FL", ""))
        .collect();
    let audio_out_base = if audio_ports.len() == 1 {
        Some(audio_ports.swap_remove(0))
    } else if audio_ports.len() > 1 {
        println!("Audio ports:");
        user_input::select_item_from_owned_collection(audio_ports)
    } else {
        None
    };

    // Connect to audio port if possible
    match audio_out_base {
        Some(port_base_name) => {
            let audio_out_left = port_base_name.clone() + &String::from("_FL");
            let audio_out_right = port_base_name.clone() + &String::from("_FR");
            match (
                active_client
                    .as_client()
                    .connect_ports_by_name(&synth_audio_out_name, &audio_out_left),
                active_client
                    .as_client()
                    .connect_ports_by_name(&synth_audio_out_name, &audio_out_right),
            ) {
                (Ok(_), Ok(_)) => {
                    println!("Successully connected audio port: {}", port_base_name);
                }
                (result1, result2) => {
                    println!(
                        "Error connecting to at least 1 port:\n{:?}\n{:?}",
                        result1, result2
                    );
                }
            }
        }
        None => {
            println!("Not connecting to audio device.\n")
        }
    }

    // Connect to midi port if available
    let ports = active_client
        .as_client()
        .ports(None, Some(".*midi.*"), jack::PortFlags::IS_OUTPUT);
    println!("Available ports:");
    match user_input::select_item_from_owned_collection(ports) {
        Some(port) => {
            let _ = active_client
                .as_client()
                .connect_ports_by_name(&port, &midi_in_name);
        }
        None => {
            println!("No midi connection was made.")
        }
    }

    let _printer_handle = std::thread::spawn(move || {
        // Print messages forever
        for message in rx {
            match message {
                SystemMessage::MidiMessage(message) => println!("MIDI IN: {message:?}"),
                SystemMessage::InfoMessage(message) => println!(" SYSTEM: {message}"),
            }
        }
    });

    // Wait for the user to kill the process
    let mut buf = [u8::default(); 1];
    let _ = io::stdin().read_exact(&mut buf);

    // Deactivate client.
    if let Ok(_) = active_client.deactivate() {
        println!("Successfully deactivated jack client.");
    } else {
        println!("Jack client could not be deactivated successfully. Jack client is destroyed.")
    }

    // Goodbye message.
    println!("Bye!");
}

/// Informational messages to be printed to the screen during runtime.
enum SystemMessage {
    MidiMessage(MidiMessage),
    InfoMessage(String),
}
